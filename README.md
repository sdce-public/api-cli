# API-CLI

In order to run the API-CLI, you must have NodeJS version 8 or above.

# Get you key

You can apply for API access by sending an email to your customer service.

Then you can generate your own key with the following URL: [https://trade.sdce.com.au/api_tokens](https://trade.sdce.com.au/api_tokens)

# API Documentation

The API documents can be found: [https://trade.sdce.com.au/documents/api_v2](https://trade.sdce.com.au/documents/api_v2)

# Usage

```
node app {ACCESS_KEY} {SECRET_KEY} {HTTP_METHOD} {FULL_URL} {DATA}
```

## Example
```
node app IK7frRFl92fAOjCJV5m0CdE0me9POYVQhwJ3JF48 yHxEem3LjOWeDcd4uMe0PHrHR7BbxAAbQyrMQYCM GET "http://localhost/api/v2/markets"
node app IK7frRFl92fAOjCJV5m0CdE0me9POYVQhwJ3JF48 yHxEem3LjOWeDcd4uMe0PHrHR7BbxAAbQyrMQYCM GET "http://localhost/api/v2/orders.json?market=btcaud&side=sell&volume=100"
```


