const request = require('request-promise');
const crypto = require('crypto');
const querystring = require('querystring');
const URL = require('url');
const process = require('process');
const argv = process.argv;

if (argv.length < 6) {
    console.log(`Usage
node app {ACCESS_KEY} {SECRET_KEY} {HTTP_METHOD} {FULL_URL} {DATA}

Example
node app IK7frRFl92fAOjCJV5m0CdE0me9POYVQhwJ3JF48 yHxEem3LjOWeDcd4uMe0PHrHR7BbxAAbQyrMQYCM GET "http://localhost/api/v2/markets"
node app IK7frRFl92fAOjCJV5m0CdE0me9POYVQhwJ3JF48 yHxEem3LjOWeDcd4uMe0PHrHR7BbxAAbQyrMQYCM GET "http://localhost/api/v2/orders.json?market=btcaud&side=sell&volume=100"
    `);
    process.exit(1);
}



accessKey = argv[2] || 'IK7frRFl92fAOjCJV5m0CdE0me9POYVQhwJ3JF48';
secret = argv[3] || 'yHxEem3LjOWeDcd4uMe0PHrHR7BbxAAbQyrMQYCM';
method = argv[4] || 'GET';
url = argv[5] || 'http://localhost/api/v2/markets';
if (argv.length > 6 && typeof argv[6] == 'string') {
    data = argv[6];
}

// GET|/api/v2/markets|access_key=IK7frRFl92fAOjCJV5m0CdE0me9POYVQhwJ3JF48&foo=bar&tonce=1534382575013

function sign(method, url, obj) {
    var u = URL.parse(url);
    var text = method.toUpperCase() + "|" + u.pathname + "|";

    var keys = Object.keys(obj);
    keys.sort(function (a, b) {
        return a > b ? 1 : a < b ? -1 : 0
    });

    var keySortedObj = {};
    
    for(var i=0; i<keys.length; i++) {
        keySortedObj[keys[i]] = obj[keys[i]];
    }

    text += querystring.stringify(keySortedObj);

    // sign!!
    const signature = crypto.createHmac('sha256', secret).update(text, 'utf-8').digest('hex');

    keySortedObj.signature = signature;
    return keySortedObj;
}

function getSignedURL(url, method) {
    var u = URL.parse(url);

    var originalQueryObject = {};
    if (u.query) {
        originalQueryObject = querystring.parse(u.query);
    }

    var queryObject = Object.assign({
        access_key: accessKey,
        tonce: (new Date).getTime()
    }, originalQueryObject);

    const keySortedObj = sign(method, u.pathname, queryObject);
    
    u.search = "?" + querystring.stringify(keySortedObj);

    const signedUrl = URL.format(u);
    return signedUrl;
}


(async function() {

    if (method == 'GET') {
        var signedUrl = getSignedURL(url, method);

        var response = await request({
            uri: signedUrl,
            method: method
        });
    
        console.log(response);
        return false;
    } else {
        var dataObject = querystring.parse(data);
        dataObject['access_key'] = accessKey;
        dataObject['tonce'] = (new Date).getTime();
        var sortedFormData = sign(method, url, dataObject)

        var response = await request({
            uri: url,
            method: method,
            formData: sortedFormData
        });

        console.log(response);
        return false;
    }

    
})()
